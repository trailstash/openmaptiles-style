import json

style = json.load(open("public/style.json"))

style["sources"]["openmaptiles"]["url"] = "https://tile.ourmap.us/data/v3.json"
style["glyphs"] = "https://trailstash.github.io/openmaptiles-fonts/fonts/{fontstack}/{range}.pbf"
style["sprite"] = "https://trailstash.gitlab.io/openmaptiles-style/sprite"

for layer in style["layers"]:
    if len(layer.get("layout", {}).get("text-font", [])) > 1:
        layer["layout"]["text-font"] = layer["layout"]["text-font"][:1]

json.dump(style, open("public/style.json", "w"), indent=2)
