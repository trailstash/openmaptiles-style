import omt from "./omt.json"

export class OpenMapTiles {
  constructor({ spriteUrl, glyphsUrl, tileUrl = "https://tile.ourmap.us/data/v3.json" }) {
    this.spriteUrl = spriteUrl;
    this.glyphsUrl = glyphsUrl;
    this.tileUrl = tileUrl;
  }
  build() {
    const style = {
      ...omt,
      glyphs: this.glyphsUrl,
      sprite: this.spriteUrl,
      sources: {
        ...omt.sources,
        openmaptiles: {
          ...omt.sources.openmaptiles,
          url: this.tileUrl,
        }
      },
      layers: omt.layers.map(layer => {
        if (layer.layout && layer.layout["text-font"]) {
          layer.layout["text-font"] = layer.layout["text-font"].slice(0, 1);
        }
        return layer;
      }),
    };
    return style;
  }
}

export default OpenMapTiles;
