# openmaptiles-style

Hosted OSM OpenMapTile Style.

This style comes from the
https://github.com/openmaptiles/openmaptiles repository. That
repository contains the code to generate this style, but it doesn't produce a hosted version of
the style or necessary fonts.

This repository hosts the style and the sprites it uses. The style loads fonts from
https://trailstash.github.io/openmaptiles-fonts and tiles from
the https://tile.ourmap.us

Simply set your style to:
```
https://trailstash.gitlab.io/openmaptiles-style/style.json
```

[Preview the style on Maputnik](https://maputnik.github.io/editor/?style=https://trailstash.gitlab.io/openmaptiles-style/style.json)
